clean:
	rm angle-lines tp-lab1.pdf tp-lab1.out tp-lab1.aux tp-lab1.log

build:
	clang -o angle-lines angle-lines.c || gcc -lm -o angle-lines angle-lines.c

test: build
	./angle-lines 3 -0.333333
	./angle-lines -1 1
	./angle-lines 0 0
	./angle-lines 1 2

TEX = pdflatex -shell-escape -interaction=nonstopmode -file-line-error
documents:
	$(TEX) tp-lab1.tex
	$(TEX) tp-lab2.tex
