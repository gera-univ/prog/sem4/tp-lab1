#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void printhelp();
void printangle();
int radianstoseconds();

static void
usage(char *argv0)
{
	printf("usage: %s k1 k2\n", argv0);
	exit(0);
}

void
printangle(double radians)
{
	int total = radianstoseconds(radians);

	int s = total % 60;
	int m = (total / 60) % 60;
	int d = total / 3600;

	if (s == 0) {
		printf("%d'", d);
		if (m != 0)
			printf("%d''", m);
	}
	else
		printf("%d:%d:%d", d, m, s);
	printf("\n");
}

int
radianstoseconds(double radians)
{
	return (int)round(radians * 360 * 60 * 60 / (2 * M_PI));
}

int main(int argc, char **argv)
{
	if (argc < 3)
		usage(argv[0]);

	double k1, k2;
	sscanf(argv[1], "%lf", &k1);
	sscanf(argv[2], "%lf", &k2);

	double angle = atan((k2 - k1)/(1.0 + k1 * k2));
	printangle(fabs(angle));

	return 0;
}
